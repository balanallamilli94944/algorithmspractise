#include<iostream>


using namespace std;




void extraLongFactorials(int n) {
    int val[1000],sz;
    val[0] = 1;
    sz = 1;
    for(int i = 2; i <= n; i++){
        int cary = 0;
        for(int j = 0; j < sz; j++){
            int pod = val[j]*i+cary;
            val[j] = pod%10;
            cary = pod/10;
        }
        while(cary){
            val[sz] = cary%10;
            cary/=10;
            sz++;
        }
    }
    for(int i = sz-1; i>= 0; i--)cout << val[i];
    cout << endl;

}

int main(int argc, char const *argv[])
{
	extraLongFactorials(200);
	return 0;
}