
#include<iostream>
#include<math.h>


using namespace std;

int jumpSearchAlgo(int arr[],int n,int target){
    int step = sqrt(n);
    int prev = 0;
    while(arr[step] < target){
        prev = step;
        step += step;
        if(step > n){
            return -1;
        }
    }
    
    while(arr[prev] < target){
        prev++;
        if(prev >= n){
            return -1;
        }
    }
    if(arr[prev] == target){
        return prev;
    }
    return -1;
}



int main(){
    
     int arr[] = { 0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377, 610 };
     int n = sizeof(arr) / sizeof(arr[0]);
     int index = jumpSearchAlgo(arr, n ,89);
     cout << index <<endl;
    return 0;
}
