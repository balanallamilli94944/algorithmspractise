#include<iostream>


using namespace std;


//works only with sorted array sorted array
// time complexity of this algorithm is O(log n);
//          0 1 2  3  4  5  6  7  8 9
// array = [2,5,8,12,16,23,38,56,72,91]
//left = 0,right = 9 mid = 0+9/2 = 9/2 = floor(9/2) = 4 => mid = 4

 
int binarySearch(int arr[],int n,int target){
    int left = 0;
    int right = n-1;
    while(left < right){
        int mid = left + right / 2;
        if(arr[mid] == target){
            return mid;
        }
        if(arr[mid] < target){
            left = mid + 1;
        }
        if(arr[mid] > target){
            right = mid - 1;
        }
    }
    return -1;
}

int binarySearch(int arr[], int l, int r, int x)
{
    if (l <= r) {
        int mid = l + r / 2;
        if (arr[mid] == x)
            return mid;
        if (arr[mid] > x)
            return binarySearch(arr, l, mid - 1, x);
        return binarySearch(arr, mid + 1, r, x);
    }
    
    // We reach here when element is not
    // present in array
    return -1;
}


int main(){
  int arr[] = {2,5,8,12,16,23,38,56,72,91};
  int n = 10;
  int index = binarySearch(arr,n,16);
  cout << index << endl;
  int idx1 = binarySearchRecursive(arr,n,0,n-1,91);
    cout << idx1 << endl;
  return 0;	
}
 
 
 
