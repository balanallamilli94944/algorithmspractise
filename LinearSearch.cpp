#include<iostream>
#include<vector>

using namespace std;


/*
 * 
 * Input : arr[] = {10, 20, 80, 30, 60, 50, 110, 100, 130, 170}
	       x = 110;
   Output : 6
Element x is present at index 6

Input : arr[] = {10, 20, 80, 30, 60, 50, 110, 100, 130, 170}
        x = 175;
Output : -1
 * */

int search(int arr[], int n, int x) 
{ 
    int i; 
    for (i = 0; i < n; i++) 
        if (arr[i] == x) 
            return i; 
    return -1; 
}


int main(){
   int arr[] = {10, 20, 80, 30, 60, 50, 110, 100, 130, 170};
   int n = sizeof(arr)/sizeof(arr[0]);
   int value = search(arr,n,110);
   if(value != -1){
	cout<<" FOUND at index "<< value << endl;
   }else{
	cout<<" element not found "<<value<<endl;
   }
   return 0;
}
