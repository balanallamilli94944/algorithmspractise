#include <iostream>
#include<vector>

using namespace std;


vector<int> v[5];

void insertionInArrayofVectors(){
    for(int i=0;i<5;i++){
        for(int j=i+1;j<5;j++){
            v[i].push_back(j);
        }
    }
}

void printElements(){
    for(int i=0;i<5;i++){
        for(auto it = v[i].begin(); it != v[i].end(); it++){
            cout << *it << " ";
        }
        cout<<endl;
    }
}

void getMissingNumber(int arr[],int n){
    int x1 = arr[0];
    int x2 = 1;
    for(int i=1;i<n;i++){
        x1 = x1 ^ arr[i];
    }
    for (int i = 2; i <= n + 1; i++){
         x2 = x2 ^ i;
    }
    cout<<" missing number in the array is "<<( x1 ^ x2 )<<endl;
    
}

int main(int argc, const char * argv[]) {
    
    insertionInArrayofVectors();
    printElements();
    int arr[] = { 1, 2, 4, 5, 6 };
    getMissingNumber(arr, 5);
    return 0;
}
