#include<iostream>

using namespace std;


//{ 12, 11, 13, 5, 6 }
//   j   i

void insertionSort(int arr[],int n){
    int key;
    int j;
    for(int i=1;i<n;i++){
        key = arr[i];
        j = i-1;
        while(j >= 0 && arr[j] > key){
            arr[j+1] = arr[j];
            j--;
        }
        arr[j+1] = key;
    }
    for(int i=0;i<n;i++){
        cout<<arr[i]<<" ";
    }
    cout<<endl;
}


int main(){
    int arr[] = {64,25,12,22,11};
    int n = 5;
    insertionSort(arr,n);
}
